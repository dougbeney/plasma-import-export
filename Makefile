all: compile run

compile:
	@gcc main.c -o plasma-ie

run:
	@./plasma-ie

KDE Plasma Import/Export
---

This project makes it easy to import/export configurations in KDE.

Features include:

- date/time based backup folder creation
- Generated map.yml file to keep track of your backups
- Patterned-based matches to ensure your most important configurations are saved


#include <stdio.h>

#define VERSION "0.0.1"

int main() {
	printf("Welcome to KDE Plasma Import/Export v.%s\n", VERSION);
	return 0;
}

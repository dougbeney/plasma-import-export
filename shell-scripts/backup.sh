#!/bin/sh
plasma_version=$(plasmashell --version | sed "s/plasmashell\s//")
current_date_time=$(date -u +"%Y-%m-%d_%H-%M")
bkup_folder="$HOME/.plasma-backups"
bkup_folder_child="$bkup_folder/$current_date_time"

# Delete child folder if exists and then create it
if [ -d "$bkup_folder_child" ]; then
	rm -r $bkup_folder_child
fi
# -p parameter will create the bkup_folder too if it does not exist
mkdir -p $bkup_folder_child

# Move all KDE-related files to the child folder
cp -r ~/.config/k* $bkup_folder_child
cp -r ~/.config/plasma* $bkup_folder_child
echo $plasma_version > $bkup_folder_child/PLASMA_VERSION.txt

cat << EOF >> $bkup_folder/map.yml
$current_date_time:
  plasma_version: $plasma_version
  location: $bkup_folder_child

EOF

echo Finished backing up your configuration for KDE Plasma version $plasma_version.

